Imports System.IO

Public Class frmMain

    Private Declare Function apiGetKeyboardState Lib "user32" Alias "GetKeyboardState" (ByVal vKeys() As Byte) As Int32
    Private Declare Function apiSetKeyboardState Lib "user32" Alias "SetKeyboardState" (ByVal vKeys() As Byte) As Int32
    Private VK(255) As Byte
    Private VK1(255) As Byte

    Public SCRN_WIDTH
    Public SCRN_HEIGHT
    Public TOTAL_BLOCKS
    Public BLOCKS_PER_LINE
    Public TOTAL_LINES

    Dim imgGameArea As Bitmap

    Dim g As Graphics

    Public BombGrid(800) As clsBombs
    Public FireGrid(800) As clsFire
    Public BricksGrid(800) As clsBricks
    Public WallGrid(800) As clsWalls
    Public BonusGrid(800) As clsBonus
    Public TrapGrid(800) As clsTrap
    Public score As Integer = 0





    Private Sub mnu2PlayerGame_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Public Sub start(ByVal field As Integer)

        If field = 1 Then

            SCRN_WIDTH = 176
            SCRN_HEIGHT = 176

            GameArea.Width = 176
            GameArea.Height = 176
            MainScreen.Width = 176
            MainScreen.Height = 176




            Me.Width = 180
            Me.Height = 225

            TOTAL_BLOCKS = (SCRN_WIDTH / BLOCK_WIDTH) * (SCRN_HEIGHT / BLOCK_HEIGHT)
            BLOCKS_PER_LINE = SCRN_WIDTH / BLOCK_WIDTH
            TOTAL_LINES = SCRN_HEIGHT / BLOCK_HEIGHT

            imgGameArea = New Bitmap(176, 176)

            life1.Location = New Point(5, SCRN_HEIGHT + 5)
            life2.Location = New Point(SCRN_WIDTH - 25, SCRN_HEIGHT + 5)
            Label1.Location = New Point(SCRN_HEIGHT / 2, SCRN_HEIGHT + 5)
            Label2.Location = New Point(SCRN_HEIGHT / 2 - 9, SCRN_HEIGHT + 5)
            stun1.Location = New Point(SCRN_WIDTH / 2 - 60, SCRN_HEIGHT)
            stun2.Location = New Point(SCRN_WIDTH / 2 + 15, SCRN_HEIGHT)
            slow1.Location = New Point(SCRN_HEIGHT / 2 - 40, SCRN_HEIGHT)
            slow2.Location = New Point(SCRN_HEIGHT / 2 + 35, SCRN_HEIGHT)
            Label3.Location = New Point(SCRN_HEIGHT / 2 + 20, SCRN_HEIGHT + 5)
            Label4.Location = New Point(SCRN_HEIGHT / 2 + 50, SCRN_HEIGHT + 5)

        ElseIf field = 2 Then

            SCRN_WIDTH = 304
            SCRN_HEIGHT = 304

            GameArea.Width = 304
            GameArea.Height = 304
            MainScreen.Width = 304
            MainScreen.Height = 304

            Me.Width = 308
            Me.Height = 304 + 49

            TOTAL_BLOCKS = (SCRN_WIDTH / BLOCK_WIDTH) * (SCRN_HEIGHT / BLOCK_HEIGHT)
            BLOCKS_PER_LINE = SCRN_WIDTH / BLOCK_WIDTH
            TOTAL_LINES = SCRN_HEIGHT / BLOCK_HEIGHT

            imgGameArea = New Bitmap(304, 304)

            life1.Location = New Point(5, SCRN_HEIGHT + 5)
            life2.Location = New Point(SCRN_WIDTH - 40, SCRN_HEIGHT + 5)
            Label1.Location = New Point(SCRN_HEIGHT - 155, SCRN_HEIGHT + 5)
            Label2.Location = New Point(SCRN_HEIGHT - 175, SCRN_HEIGHT + 5)
            stun1.Location = New Point(SCRN_WIDTH - 255, SCRN_HEIGHT)
            stun2.Location = New Point(SCRN_WIDTH - 85, SCRN_HEIGHT)
            slow1.Location = New Point(SCRN_HEIGHT - 235, SCRN_HEIGHT)
            slow2.Location = New Point(SCRN_HEIGHT - 105, SCRN_HEIGHT)
            Label3.Location = New Point(SCRN_HEIGHT - 105, SCRN_HEIGHT + 5)
            Label4.Location = New Point(SCRN_WIDTH - 65, SCRN_HEIGHT + 5)
        ElseIf field = 3 Then

            SCRN_WIDTH = 400
            SCRN_HEIGHT = 400

            GameArea.Width = 400
            GameArea.Height = 400
            MainScreen.Width = 400
            MainScreen.Height = 400



            Me.Width = 404
            Me.Height = 400 + 49

            TOTAL_BLOCKS = (SCRN_WIDTH / BLOCK_WIDTH) * (SCRN_HEIGHT / BLOCK_HEIGHT)
            BLOCKS_PER_LINE = SCRN_WIDTH / BLOCK_WIDTH
            TOTAL_LINES = SCRN_HEIGHT / BLOCK_HEIGHT

            imgGameArea = New Bitmap(400, 400)

            life1.Location = New Point(5, SCRN_HEIGHT + 5)
            life2.Location = New Point(SCRN_WIDTH - 40, SCRN_HEIGHT + 5)
            Label1.Location = New Point(SCRN_HEIGHT - 190, SCRN_HEIGHT + 5)
            Label2.Location = New Point(SCRN_HEIGHT - 210, SCRN_HEIGHT + 5)
            stun1.Location = New Point(SCRN_WIDTH - 290, SCRN_HEIGHT)
            stun2.Location = New Point(SCRN_WIDTH - 110, SCRN_HEIGHT)
            slow1.Location = New Point(SCRN_HEIGHT - 270, SCRN_HEIGHT)
            slow2.Location = New Point(SCRN_HEIGHT - 130, SCRN_HEIGHT)

            Label3.Location = New Point(SCRN_WIDTH - 110, SCRN_HEIGHT + 5)
            Label4.Location = New Point(SCRN_WIDTH - 60, SCRN_HEIGHT + 5)

        End If


        ext.Location = New Point(SCRN_HEIGHT / 2 - 30, SCRN_HEIGHT / 2 - 20)

        restart.Location = New Point(SCRN_HEIGHT / 2 - 30, SCRN_HEIGHT / 2)

        gotomenu.Location = New Point(SCRN_HEIGHT / 2 - 30, SCRN_HEIGHT / 2 + 20)


        g = Graphics.FromImage(imgGameArea)


        InitializeArrays()

        LoadKeyConfiguration()

        LoadImages()


        If TotalPlayers = 1 Then
            Form4.ComboBox1.Text = 30
            slow2.Hide()
            stun2.Hide()
            life2.Hide()
        Else
            Label3.Hide()
            Label4.Hide()
        End If

        Timer1.Enabled = True
        StartNewGame()



        ext.Hide()

        restart.Hide()

        gotomenu.Hide()

        slow1.Visible = False
        slow2.Visible = False
        stun1.Visible = False
        stun2.Visible = False

        Dim lama = CInt(Form4.ComboBox1.Text)

        Dim menit As Integer = lama / 60

        Dim detik As Integer = lama - (60 * menit)

        Label1.Text = detik
        Label2.Text = menit

    End Sub



    Private Sub Timer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer.Tick

        Dim XY As Point

        If GameOver Then Exit Sub

        If GameTimer Mod 3 = 0 Then UpdateFire()
        If GameTimer Mod 10 = 0 Then UpdateBomb()
        If GameTimer Mod 10 = 0 Then UpdateFireBrick()
        If GameTimer Mod 10 = 0 Then UpdateDeadBombermans()
        If GameTimer Mod 100 = 0 Then CheckIsGameFinished()

        'HandleInput()



            For i As Integer = 0 To TotalPlayers - 1

                Dim x As Integer = BomberMan(i).X
                Dim y As Integer = BomberMan(i).Y


                If BomberMan(i).IsAlive = True Then
                If IsBombermanCollidingWithFire(BomberMan(0).X, BomberMan(0).Y) Then
                    If Form2.p1 = 1 Then
                        BomberMan(0).Die(2)
                        life1.Text = BomberMan(0).live
                    ElseIf Form2.p1 = 2 Then
                        BomberMan(0).Die(2)
                        life1.Text = BomberMan(0).live
                    ElseIf Form2.p1 = 3 Then
                        BomberMan(0).Die(2)
                        life1.Text = BomberMan(0).live
                    ElseIf Form2.p1 = 4 Then
                        BomberMan(0).Die(1)
                        life1.Text = BomberMan(0).live
                    End If

                ElseIf IsBombermanCollidingWithFire(BomberMan(1).X, BomberMan(1).Y) Then
                    If Form3.p2 = 1 Then
                        BomberMan(1).Die(2)
                        life2.Text = BomberMan(1).live
                    ElseIf Form3.p2 = 2 Then
                        BomberMan(1).Die(2)
                        life2.Text = BomberMan(1).live
                    ElseIf Form3.p2 = 3 Then
                        BomberMan(1).Die(2)
                        life2.Text = BomberMan(1).live
                    ElseIf Form3.p2 = 4 Then
                        BomberMan(1).Die(1)
                        life2.Text = BomberMan(1).live
                    End If
                End If
                End If

            For a As Integer = 0 To TOTAL_BLOCKS
                If BonusGrid(a).IsEmpty = False Then

                    XY = GetXYFromBlock(a)
                    If ((y + 14 <= XY.Y + BLOCK_HEIGHT) And (y + BOMBERMAN_HEIGHT > XY.Y)) And ((x + BOMBERMAN_WIDTH >= XY.X + 4) And (x + 4 <= XY.X + BLOCK_WIDTH)) Then
                        If BonusGrid(a).BonusType = clsBonus.Bonuses.BOMB Then
                            BomberMan(i).BombsLeft += 1
                        Else
                            BomberMan(i).Strength += 1
                        End If
                        BonusGrid(a).RemoveBonus()
                    End If

                End If
            Next a

            For t As Integer = 0 To TOTAL_BLOCKS

                If TrapGrid(t).IsEmpty = False Then
                    XY = GetXYFromBlock(t)
                    If ((y + 14 <= XY.Y + BLOCK_HEIGHT) And (y + BOMBERMAN_HEIGHT > XY.Y)) And ((x + BOMBERMAN_WIDTH >= XY.X + 4) And (x + 4 <= XY.X + BLOCK_WIDTH)) Then
                        If TrapGrid(t).TrapType = clsTrap.Trap.Stun Then
                            BomberMan(i).isStun = True
                            If i = 0 Then
                                stun1.Visible = True
                            Else
                                stun2.Visible = True
                            End If
                        Else
                            BomberMan(i).isSlow = True

                            If i = 0 Then
                                slow1.Visible = True
                                timerPlayer1.Interval = 100
                            Else
                                slow2.Visible = True
                                timerPlayer2.Interval = 100
                            End If
                        End If
                        TrapGrid(t).RemoveTrap()
                    End If

                End If
            Next t

            If BomberMan(i).isSlow = True Then
                BomberMan(i).slowDuration -= 1
                If BomberMan(i).slowDuration <= 0 Then

                    If i = 0 Then
                        slow1.Visible = False
                        timerPlayer1.Interval = BomberMan(i).speed
                    Else
                        slow2.Visible = False
                        timerPlayer2.Interval = BomberMan(i).speed
                    End If
                    BomberMan(i).slowDuration = 200
                End If
            End If


        Next i


            GameTimer += 1


            Draw()

    End Sub



    Public Sub StartNewGame()

        EmptyGrids()
        FillWalls()
        FillBricks()
        FillBonus()
        FillTrap()

        InitializeBombermans()
        If (Form2.p1 = 1) Then
            BomberMan(0).live = 375
            BomberMan(0).speed = 30



        ElseIf (Form2.p1 = 2) Then
            BomberMan(0).live = 300
            BomberMan(0).speed = 1
        ElseIf (Form2.p1 = 3) Then
            BomberMan(0).live = 500
            BomberMan(0).speed = 40
        ElseIf (Form2.p1 = 4) Then
            BomberMan(0).live = 300
            BomberMan(0).speed = 40

        End If

        If (Form3.p2 = 1) Then
            BomberMan(1).live = 375
            BomberMan(1).speed = 30


        ElseIf (Form3.p2 = 2) Then
            BomberMan(1).live = 300
            BomberMan(1).speed = 1

        ElseIf (Form3.p2 = 3) Then
            BomberMan(1).live = 500
            BomberMan(1).speed = 40

        ElseIf (Form3.p2 = 4) Then
            BomberMan(1).live = 300
            BomberMan(1).speed = 40

        End If

        life1.Text = BomberMan(0).live
        life2.Text = BomberMan(1).live
        timerPlayer1.Interval = BomberMan(0).speed
        timerPlayer2.Interval = BomberMan(1).speed


        GameOver = False
        GameTimer = 0

        GameArea.Show()
        Timer.Enabled = True
        timerPlayer1.Enabled = True
        timerPlayer2.Enabled = True

    End Sub



    Private Sub Draw()

        Dim XY As Point
        g.Clear(Color.Transparent)



        For i As Integer = 0 To TOTAL_BLOCKS
            If BombGrid(i).IsEmpty = False Then
                XY = GetXYFromBlock(i)
                If BombGrid(i).TimeElapsed Mod 2 = 0 Then
                    g.DrawImage(imgBomb1, XY.X, XY.Y)
                Else
                    g.DrawImage(imgBomb1, XY.X, XY.Y)
                End If
            End If
        Next



        For i As Integer = 0 To TOTAL_BLOCKS
            If BonusGrid(i).IsEmpty = False Then
                XY = GetXYFromBlock(i)
                If BonusGrid(i).BonusType = clsBonus.Bonuses.BOMB Then
                    g.DrawImage(imgBonusBomb, XY.X, XY.Y, 16, 16)
                Else
                    g.DrawImage(imgBonusFire, XY.X, XY.Y, 16, 16)
                End If
            End If
        Next i



        For i As Integer = 0 To TOTAL_BLOCKS
            If FireGrid(i).IsEmpty = False Then
                Dim direction As String = FireGrid(i).Direction
                Dim state As Integer = FireGrid(i).State

                XY = GetXYFromBlock(i)

                Select Case direction
                    Case "C" : g.DrawImage(imgFireCenter(state - 1), XY.X, XY.Y)
                    Case "H" : g.DrawImage(imgFireHorizontal(state - 1), XY.X, XY.Y)
                    Case "V" : g.DrawImage(imgFireVertical(state - 1), XY.X, XY.Y)
                    Case "E" : g.DrawImage(imgFireEast(state - 1), XY.X, XY.Y)
                    Case "W" : g.DrawImage(imgFireWest(state - 1), XY.X, XY.Y)
                    Case "S" : g.DrawImage(imgFireSouth(state - 1), XY.X, XY.Y)
                    Case "N" : g.DrawImage(imgFireNorth(state - 1), XY.X, XY.Y)
                End Select

            End If
        Next



        For i As Integer = 0 To TOTAL_BLOCKS
            If BricksGrid(i).IsEmpty = False Then
                XY = GetXYFromBlock(i)

                If BricksGrid(i).IsOnFire = True Then
                    g.DrawImage(imgFireBricks(BricksGrid(i).FireState), XY.X, XY.Y)
                Else
                    g.DrawImage(imgBrick, XY.X, XY.Y)
                End If
            End If
        Next



        For i As Integer = 0 To TOTAL_BLOCKS
            If WallGrid(i).IsEmpty = False Then
                XY = GetXYFromBlock(i)
                g.DrawImage(imgWall, XY.X, XY.Y, 16, 16)
            End If
        Next i




        For i As Integer = 0 To TotalPlayers - 1
            Dim state As Integer = BomberMan(i).State
            Dim X As Integer = BomberMan(i).X
            Dim Y As Integer = BomberMan(i).Y

            If BomberMan(i).IsAlive = True Then
                If BomberMan(i).Direction = clsBomberman.Directions.UP Then g.DrawImage(BomberMan(i).imgUp(state), X, Y, BOMBERMAN_WIDTH, BOMBERMAN_HEIGHT)
                If BomberMan(i).Direction = clsBomberman.Directions.DOWN Then g.DrawImage(BomberMan(i).imgDown(state), X, Y, BOMBERMAN_WIDTH, BOMBERMAN_HEIGHT)
                If BomberMan(i).Direction = clsBomberman.Directions.LEFT Then g.DrawImage(BomberMan(i).imgLeft(state), X, Y, BOMBERMAN_WIDTH, BOMBERMAN_HEIGHT)
                If BomberMan(i).Direction = clsBomberman.Directions.RIGHT Then g.DrawImage(BomberMan(i).imgRight(state), X, Y, BOMBERMAN_WIDTH, BOMBERMAN_HEIGHT)
            Else

                If state < 5 Then
                    g.DrawImage(BomberMan(i).imgDie(state), X, Y, BOMBERMAN_WIDTH, BOMBERMAN_HEIGHT)
                End If
            End If
        Next



        GameArea.Image = imgGameArea

    End Sub




    Private Sub UpdateFireBrick()

        Dim count As Integer = 0

        For i As Integer = 0 To TOTAL_BLOCKS
            If BricksGrid(i).IsEmpty = False Then

                If BricksGrid(i).IsOnFire = True Then
                    If BricksGrid(i).FireState < 4 Then
                        BricksGrid(i).FireState += 1
                    Else
                        BricksGrid(i).RemoveBrick()
                        count += 1
                    End If
                End If

            End If
        Next
        If count > 0 Then
            Label4.Text = (CInt(Label4.Text) + 10).ToString
        End If



    End Sub



    Private Sub UpdateFire()

        For i As Integer = 0 To TOTAL_BLOCKS
            If FireGrid(i).IsEmpty = False Then
                If FireGrid(i).State < 8 Then
                    FireGrid(i).State += 1
                Else
                    FireGrid(i).RemoveFire()
                End If
            End If
        Next

    End Sub



    Private Sub UpdateBomb()
        For i As Integer = 0 To TOTAL_BLOCKS
            If BombGrid(i).IsEmpty = False Then
                If BombGrid(i).TimeElapsed < 4 Then
                    BombGrid(i).TimeElapsed += 1
                Else
                    BlastBomb(i)
                End If
            End If
        Next

    End Sub



    Public Sub UpdateDeadBombermans()

        For i As Integer = 0 To TotalPlayers - 1
            If (BomberMan(i).IsAlive = False) And (BomberMan(i).State < 5) Then
                BomberMan(i).State += 1
            End If
        Next

    End Sub


    
    Private Sub BlastBomb(ByVal block As Integer)

        Dim canMoveN As Boolean = True, canMoveS = True, canMoveE = True, canMoveW = True
        Dim NewBlock As Integer
        Dim Player As Integer


        Player = BombGrid(block).Player

        BombGrid(block).RemoveBomb()
        BomberMan(Player).BombsLeft += 1

        FireGrid(block).InsertFire("C")



        For x As Integer = 1 To BomberMan(Player).Strength
            If canMoveN = True Then
                NewBlock = block - (BLOCKS_PER_LINE * x)

                If BricksGrid(NewBlock).IsEmpty = False Then
                    BricksGrid(NewBlock).IsOnFire = True
                    canMoveN = False
                End If


                If BombGrid(NewBlock).IsEmpty = False Then
                    BlastBomb(NewBlock)
                End If


                If WallGrid(NewBlock).IsEmpty = True Then

                    If FireGrid(NewBlock).IsEmpty = True Then

                        If (x <> BomberMan(Player).Strength) Then
                            FireGrid(NewBlock).InsertFire("V")
                        Else
                            FireGrid(NewBlock).InsertFire("N")
                        End If
                    End If
                Else
                    canMoveN = False
                End If
            End If



            If canMoveS = True Then
                NewBlock = block + (BLOCKS_PER_LINE * x)

                If BricksGrid(NewBlock).IsEmpty = False Then
                    BricksGrid(NewBlock).IsOnFire = True
                    canMoveS = False
                End If

                If BombGrid(NewBlock).IsEmpty = False Then
                    BlastBomb(NewBlock)
                End If

                If WallGrid(NewBlock).IsEmpty = True Then
                    If FireGrid(NewBlock).IsEmpty = True Then
                        If (x <> BomberMan(Player).Strength) Then
                            FireGrid(NewBlock).InsertFire("V")
                        Else
                            FireGrid(NewBlock).InsertFire("S")
                        End If
                    End If
                Else
                    canMoveS = False
                End If
            End If


            If canMoveE = True Then
                NewBlock = block + x

                If BricksGrid(NewBlock).IsEmpty = False Then
                    BricksGrid(NewBlock).IsOnFire = True
                    canMoveE = False
                End If

                If BombGrid(NewBlock).IsEmpty = False Then
                    BlastBomb(NewBlock)
                End If

                If WallGrid(NewBlock).IsEmpty = True Then
                    If FireGrid(NewBlock).IsEmpty = True Then
                        If (x <> BomberMan(Player).Strength) Then
                            FireGrid(NewBlock).InsertFire("H")
                        Else
                            FireGrid(NewBlock).InsertFire("E")
                        End If
                    End If
                Else
                    canMoveE = False
                End If
            End If


            If canMoveW = True Then
                NewBlock = block - x

                If BricksGrid(NewBlock).IsEmpty = False Then
                    BricksGrid(NewBlock).IsOnFire = True
                    canMoveW = False
                End If

                If BombGrid(NewBlock).IsEmpty = False Then
                    BlastBomb(NewBlock)
                End If

                If WallGrid(NewBlock).IsEmpty = True Then
                    If FireGrid(NewBlock).IsEmpty = True Then
                        If (x <> BomberMan(Player).Strength) Then
                            FireGrid(NewBlock).InsertFire("H")
                        Else
                            FireGrid(NewBlock).InsertFire("W")
                        End If
                    End If
                Else
                    canMoveW = False
                End If
            End If

        Next


    End Sub



    Public Sub HandleInput(ByVal i As Integer)

        Dim NewX As Integer
        Dim NewY As Integer
        Dim Move As Integer = 3

            If BomberMan(i).IsAlive = True And BomberMan(i).isStun = False Then

                If IsKeyPressed(BomberMan(i).KEY_UP) Then
                    NewX = BomberMan(i).X
                    NewY = BomberMan(i).Y - Move

                    If IsBombermanColliding(NewX, NewY) = False Then


                        If BomberMan(i).Direction = clsBomberman.Directions.UP Then
                            BomberMan(i).State += 1
                            BomberMan(i).State = BomberMan(i).State Mod 5
                        Else
                            BomberMan(i).Direction = clsBomberman.Directions.UP
                            BomberMan(i).State = 0
                        End If

                        BomberMan(i).X = NewX
                        BomberMan(i).Y = NewY
                    End If
                End If

                If IsKeyPressed(BomberMan(i).KEY_DOWN) Then
                    NewX = BomberMan(i).X
                    NewY = BomberMan(i).Y + Move

                    If IsBombermanColliding(NewX, NewY) = False Then

                        If BomberMan(i).Direction = clsBomberman.Directions.DOWN Then
                            BomberMan(i).State += 1
                            BomberMan(i).State = BomberMan(i).State Mod 5
                        Else
                            BomberMan(i).Direction = clsBomberman.Directions.DOWN
                            BomberMan(i).State = 0
                        End If

                        BomberMan(i).X = NewX
                        BomberMan(i).Y = NewY
                    End If
                End If


                If IsKeyPressed(BomberMan(i).KEY_LEFT) Then
                    NewX = BomberMan(i).X - Move
                    NewY = BomberMan(i).Y

                    If IsBombermanColliding(NewX, NewY) = False Then

                        If BomberMan(i).Direction = clsBomberman.Directions.LEFT Then
                            BomberMan(i).State += 1
                            BomberMan(i).State = BomberMan(i).State Mod 5
                        Else
                            BomberMan(i).Direction = clsBomberman.Directions.LEFT
                            BomberMan(i).State = 0
                        End If

                        BomberMan(i).X = NewX
                        BomberMan(i).Y = NewY
                    End If
                End If


                If IsKeyPressed(BomberMan(i).KEY_RIGHT) Then
                    NewX = BomberMan(i).X + Move
                    NewY = BomberMan(i).Y

                    If IsBombermanColliding(NewX, NewY) = False Then

                        If BomberMan(i).Direction = clsBomberman.Directions.RIGHT Then
                            BomberMan(i).State += 1
                            BomberMan(i).State = BomberMan(i).State Mod 5
                        Else
                            BomberMan(i).Direction = clsBomberman.Directions.RIGHT
                            BomberMan(i).State = 0
                        End If

                        BomberMan(i).X = NewX
                        BomberMan(i).Y = NewY
                    End If
                End If


                If IsKeyToggled(BomberMan(i).KEY_BOMB) Then
                    If BomberMan(i).BombsLeft > 0 Then
                        Dim BomberBlock As Integer = GetBlockFromXY(BomberMan(i).X, BomberMan(i).Y + 12)
                        BombGrid(BomberBlock).InsertBomb(i)
                        BomberMan(i).BombsLeft -= 1
                    End If
                End If

            Else
                BomberMan(i).stunDuration -= 1
            If BomberMan(i).stunDuration <= 0 Then
                BomberMan(i).isStun = False
                BomberMan(i).stunDuration = 300
                If i = 0 Then
                    stun1.Visible = False

                Else
                    stun2.Visible = False

                End If
            End If
        End If

       


    End Sub


    Public Function IsKeyPressed(ByVal key As Keys) As Boolean

        apiGetKeyboardState(VK)
        If (VK(key) = 128) Or (VK(key) = 129) Then
            Return True
        Else
            Return False
        End If

    End Function



    Public Function IsKeyToggled(ByVal key As Keys) As Boolean

        apiGetKeyboardState(VK)
        If (VK(key) = 128) Or (VK(key) = 129) Then
            If (VK(key) <> VK1(key)) Or (VK1(key) = 0) Then
                VK1(key) = VK(key)
                Return True
            End If
        End If

        Return False

    End Function


    Public Sub CheckIsGameFinished()


        Dim PlayersLeft As Integer = 0
        Dim Player As Integer



        For i As Integer = 0 To TotalPlayers - 1
            If BomberMan(i).IsAlive = True Then
                PlayersLeft += 1
                Player = i
            End If
        Next

        If TotalPlayers > 1 Then

            If PlayersLeft = 1 Then
                Timer.Enabled = False
                timerPlayer1.Enabled = False
                timerPlayer2.Enabled = False
                Timer1.Enabled = False
                GameArea.Hide()
                MainScreen.BackgroundImage = ImageList2.Images(Player & ".png")
                GameOver = True
                ext.Show()

                restart.Show()

                gotomenu.Show()
            End If

            If PlayersLeft = 0 Then
                Timer.Enabled = False
                timerPlayer1.Enabled = False
                timerPlayer2.Enabled = False
                Timer1.Enabled = False
                GameArea.Hide()
                MainScreen.BackgroundImage = ImageList2.Images("draw")
                GameOver = True
                ext.Show()

                restart.Show()

                gotomenu.Show()
            End If
        Else
            If PlayersLeft = 0 Then
                Timer.Enabled = False
                GameArea.Hide()
                GameOver = True
                Form7.scoreFinal = Label4.Text
                Me.Close()
                Form7.Show()

            End If
        End If

    End Sub



    Public Sub LoadImages()

        For i As Integer = 0 To 7
            imgFireWest(i) = New Bitmap(ImageList.Images("W" & i + 1 & ".gif"))
            imgFireEast(i) = New Bitmap(ImageList.Images("E" & i + 1 & ".gif"))
            imgFireNorth(i) = New Bitmap(ImageList.Images("N" & i + 1 & ".gif"))
            imgFireSouth(i) = New Bitmap(ImageList.Images("S" & i + 1 & ".gif"))
            imgFireHorizontal(i) = New Bitmap(ImageList.Images("H" & i + 1 & ".gif"))
            imgFireVertical(i) = New Bitmap(ImageList.Images("V" & i + 1 & ".gif"))
            imgFireCenter(i) = New Bitmap(ImageList.Images("C" & i + 1 & ".gif"))
        Next

        imgBomb1 = New Bitmap(ImageList.Images("bomb1"))
    

        imgBrick = New Bitmap(ImageList.Images("brick.png"))

        For i As Integer = 0 To 4
            imgFireBricks(i) = New Bitmap(ImageList.Images("firebrick" & i))
        Next


        imgWall = New Bitmap(ImageList.Images("wall.png"))


        For x As Integer = 0 To 1
            For i As Integer = 1 To 5
                BomberMan(x).imgUp(i - 1) = New Bitmap(ImageList1.Images(x & "13.png"))
            Next i
            For i As Integer = 6 To 10
                BomberMan(x).imgDown(i - 6) = New Bitmap(ImageList1.Images(x & "1.png"))
            Next i
            For i As Integer = 11 To 15
                BomberMan(x).imgLeft(i - 11) = New Bitmap(ImageList1.Images(x & "5.png"))
            Next i
            For i As Integer = 16 To 20
                BomberMan(x).imgRight(i - 16) = New Bitmap(ImageList1.Images(x & "9.png"))
            Next i
            For i As Integer = 21 To 25
                BomberMan(x).imgDie(i - 21) = New Bitmap(ImageList1.Images(x & "1.png"))
            Next i
        Next x


        imgBonusBomb = New Bitmap(ImageList.Images("bonusbomb1"))
        imgBonusFire = New Bitmap(ImageList.Images("bonusfire1"))

    End Sub

    Private Sub restart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles restart.Click
        InitializeArrays()

        LoadKeyConfiguration()

        LoadImages()

        TotalPlayers = TotalPlayers

        StartNewGame()

        ext.Hide()

        restart.Hide()

        gotomenu.Hide()
        Timer1.Enabled = True

        Dim lama = CInt(Form4.ComboBox1.Text)

        Dim menit As Integer = lama / 60

        Dim detik As Integer = lama - (60 * menit)

        Label1.Text = detik
        Label2.Text = menit
    End Sub

    Private Sub gotomenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gotomenu.Click
        Me.Close()
        Form1.Show()

    End Sub

    Private Sub ext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ext.Click
        Me.Close()
    End Sub


    Private Sub timerPlayer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timerPlayer1.Tick
        HandleInput(0)
    End Sub

    Private Sub timerPlayer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timerPlayer2.Tick
        HandleInput(1)
    End Sub

 
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim a As Integer
        Dim b As Integer
        Dim lbl2 As Integer

        a = Label1.Text
        b = 1
        lbl2 = Label2.Text
        Label1.Text = a - b

        If TotalPlayers = 1 Then

            If Label2.Text = "0" And Label1.Text = "0" Then

                Timer.Enabled = False
                GameArea.Hide()
                GameOver = True
                Form7.scoreFinal = Label4.Text
                Me.Close()
                Form7.Show()
                
            ElseIf Label1.Text = "0" Or Label1.Text = "-1" Then
                Label2.Text = lbl2 - b
                Label1.Text = 59
            End If

        Else
            If Label2.Text = "0" And Label1.Text = "0" Then

                Timer.Enabled = False
                Timer1.Enabled = False
                timerPlayer1.Enabled = False
                timerPlayer2.Enabled = False
                GameArea.Hide()
                MainScreen.BackgroundImage = ImageList2.Images("draw")
                GameOver = True
                ext.Show()

                restart.Show()

                gotomenu.Show()

            ElseIf Label1.Text = "0" or Label1.Text = "-1" Then
                Label2.Text = lbl2 - b
                Label1.Text = 59
            End If

        End If
    End Sub

   
    Private Sub GameArea_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GameArea.Click

    End Sub
End Class
