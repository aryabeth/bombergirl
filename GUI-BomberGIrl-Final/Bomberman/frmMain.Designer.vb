<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.ImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.Timer = New System.Windows.Forms.Timer(Me.components)
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.restart = New System.Windows.Forms.Button()
        Me.ext = New System.Windows.Forms.Button()
        Me.gotomenu = New System.Windows.Forms.Button()
        Me.life1 = New System.Windows.Forms.Label()
        Me.life2 = New System.Windows.Forms.Label()
        Me.timerPlayer1 = New System.Windows.Forms.Timer(Me.components)
        Me.timerPlayer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.slow1 = New System.Windows.Forms.Label()
        Me.slow2 = New System.Windows.Forms.Label()
        Me.stun2 = New System.Windows.Forms.Label()
        Me.stun1 = New System.Windows.Forms.Label()
        Me.GameArea = New System.Windows.Forms.PictureBox()
        Me.MainScreen = New System.Windows.Forms.PictureBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        CType(Me.GameArea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MainScreen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList.Images.SetKeyName(0, "bomb1")
        Me.ImageList.Images.SetKeyName(1, "bonusbomb1")
        Me.ImageList.Images.SetKeyName(2, "bonusbomb2")
        Me.ImageList.Images.SetKeyName(3, "bonusfire1")
        Me.ImageList.Images.SetKeyName(4, "bonusfire2")
        Me.ImageList.Images.SetKeyName(5, "firebrick0")
        Me.ImageList.Images.SetKeyName(6, "firebrick1")
        Me.ImageList.Images.SetKeyName(7, "firebrick2")
        Me.ImageList.Images.SetKeyName(8, "firebrick3")
        Me.ImageList.Images.SetKeyName(9, "firebrick4")
        Me.ImageList.Images.SetKeyName(10, "wall")
        Me.ImageList.Images.SetKeyName(11, "C1.gif")
        Me.ImageList.Images.SetKeyName(12, "C2.gif")
        Me.ImageList.Images.SetKeyName(13, "C3.gif")
        Me.ImageList.Images.SetKeyName(14, "C4.gif")
        Me.ImageList.Images.SetKeyName(15, "C5.gif")
        Me.ImageList.Images.SetKeyName(16, "C6.gif")
        Me.ImageList.Images.SetKeyName(17, "C7.gif")
        Me.ImageList.Images.SetKeyName(18, "C8.gif")
        Me.ImageList.Images.SetKeyName(19, "E1.gif")
        Me.ImageList.Images.SetKeyName(20, "E2.gif")
        Me.ImageList.Images.SetKeyName(21, "E3.gif")
        Me.ImageList.Images.SetKeyName(22, "E4.gif")
        Me.ImageList.Images.SetKeyName(23, "E5.gif")
        Me.ImageList.Images.SetKeyName(24, "E6.gif")
        Me.ImageList.Images.SetKeyName(25, "E7.gif")
        Me.ImageList.Images.SetKeyName(26, "E8.gif")
        Me.ImageList.Images.SetKeyName(27, "H1.gif")
        Me.ImageList.Images.SetKeyName(28, "H2.gif")
        Me.ImageList.Images.SetKeyName(29, "H3.gif")
        Me.ImageList.Images.SetKeyName(30, "H4.gif")
        Me.ImageList.Images.SetKeyName(31, "H5.gif")
        Me.ImageList.Images.SetKeyName(32, "H6.gif")
        Me.ImageList.Images.SetKeyName(33, "H7.gif")
        Me.ImageList.Images.SetKeyName(34, "H8.gif")
        Me.ImageList.Images.SetKeyName(35, "N1.gif")
        Me.ImageList.Images.SetKeyName(36, "N2.gif")
        Me.ImageList.Images.SetKeyName(37, "N3.gif")
        Me.ImageList.Images.SetKeyName(38, "N4.gif")
        Me.ImageList.Images.SetKeyName(39, "N5.gif")
        Me.ImageList.Images.SetKeyName(40, "N6.gif")
        Me.ImageList.Images.SetKeyName(41, "N7.gif")
        Me.ImageList.Images.SetKeyName(42, "N8.gif")
        Me.ImageList.Images.SetKeyName(43, "S1.gif")
        Me.ImageList.Images.SetKeyName(44, "S2.gif")
        Me.ImageList.Images.SetKeyName(45, "S3.gif")
        Me.ImageList.Images.SetKeyName(46, "S4.gif")
        Me.ImageList.Images.SetKeyName(47, "S5.gif")
        Me.ImageList.Images.SetKeyName(48, "S6.gif")
        Me.ImageList.Images.SetKeyName(49, "S7.gif")
        Me.ImageList.Images.SetKeyName(50, "S8.gif")
        Me.ImageList.Images.SetKeyName(51, "V1.gif")
        Me.ImageList.Images.SetKeyName(52, "V2.gif")
        Me.ImageList.Images.SetKeyName(53, "V3.gif")
        Me.ImageList.Images.SetKeyName(54, "V4.gif")
        Me.ImageList.Images.SetKeyName(55, "V5.gif")
        Me.ImageList.Images.SetKeyName(56, "V6.gif")
        Me.ImageList.Images.SetKeyName(57, "V7.gif")
        Me.ImageList.Images.SetKeyName(58, "V8.gif")
        Me.ImageList.Images.SetKeyName(59, "W1.gif")
        Me.ImageList.Images.SetKeyName(60, "W2.gif")
        Me.ImageList.Images.SetKeyName(61, "W3.gif")
        Me.ImageList.Images.SetKeyName(62, "W4.gif")
        Me.ImageList.Images.SetKeyName(63, "W5.gif")
        Me.ImageList.Images.SetKeyName(64, "W6.gif")
        Me.ImageList.Images.SetKeyName(65, "W7.gif")
        Me.ImageList.Images.SetKeyName(66, "W8.gif")
        Me.ImageList.Images.SetKeyName(67, "brick")
        Me.ImageList.Images.SetKeyName(68, "wall.png")
        Me.ImageList.Images.SetKeyName(69, "brick.png")
        '
        'Timer
        '
        Me.Timer.Interval = 1
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "01.gif")
        Me.ImageList1.Images.SetKeyName(1, "11.gif")
        Me.ImageList1.Images.SetKeyName(2, "01.png")
        Me.ImageList1.Images.SetKeyName(3, "05.png")
        Me.ImageList1.Images.SetKeyName(4, "09.png")
        Me.ImageList1.Images.SetKeyName(5, "013.png")
        Me.ImageList1.Images.SetKeyName(6, "11.png")
        Me.ImageList1.Images.SetKeyName(7, "15.png")
        Me.ImageList1.Images.SetKeyName(8, "19.png")
        Me.ImageList1.Images.SetKeyName(9, "113.png")
        '
        'ImageList2
        '
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList2.Images.SetKeyName(0, "0")
        Me.ImageList2.Images.SetKeyName(1, "1")
        Me.ImageList2.Images.SetKeyName(2, "draw")
        Me.ImageList2.Images.SetKeyName(3, "Enter to Continue.jpg")
        Me.ImageList2.Images.SetKeyName(4, "0.png")
        Me.ImageList2.Images.SetKeyName(5, "1.png")
        '
        'restart
        '
        Me.restart.Location = New System.Drawing.Point(125, 196)
        Me.restart.Name = "restart"
        Me.restart.Size = New System.Drawing.Size(75, 23)
        Me.restart.TabIndex = 2
        Me.restart.Text = "Play Again"
        Me.restart.UseVisualStyleBackColor = True
        '
        'ext
        '
        Me.ext.Location = New System.Drawing.Point(125, 254)
        Me.ext.Name = "ext"
        Me.ext.Size = New System.Drawing.Size(75, 23)
        Me.ext.TabIndex = 3
        Me.ext.Text = "Exit"
        Me.ext.UseVisualStyleBackColor = True
        '
        'gotomenu
        '
        Me.gotomenu.Location = New System.Drawing.Point(125, 225)
        Me.gotomenu.Name = "gotomenu"
        Me.gotomenu.Size = New System.Drawing.Size(75, 23)
        Me.gotomenu.TabIndex = 4
        Me.gotomenu.Text = "Go to Menu"
        Me.gotomenu.UseVisualStyleBackColor = True
        '
        'life1
        '
        Me.life1.AutoSize = True
        Me.life1.Location = New System.Drawing.Point(12, 325)
        Me.life1.Name = "life1"
        Me.life1.Size = New System.Drawing.Size(13, 13)
        Me.life1.TabIndex = 5
        Me.life1.Text = "3"
        '
        'life2
        '
        Me.life2.AutoSize = True
        Me.life2.Location = New System.Drawing.Point(294, 325)
        Me.life2.Name = "life2"
        Me.life2.Size = New System.Drawing.Size(13, 13)
        Me.life2.TabIndex = 6
        Me.life2.Text = "3"
        '
        'timerPlayer1
        '
        Me.timerPlayer1.Interval = 1
        '
        'timerPlayer2
        '
        Me.timerPlayer2.Interval = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(147, 334)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(19, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "30"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(122, 334)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(13, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "0"
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'slow1
        '
        Me.slow1.Image = Global.Bomberman.My.Resources.Resources.slow
        Me.slow1.Location = New System.Drawing.Point(45, 334)
        Me.slow1.Name = "slow1"
        Me.slow1.Size = New System.Drawing.Size(28, 23)
        Me.slow1.TabIndex = 12
        '
        'slow2
        '
        Me.slow2.Image = Global.Bomberman.My.Resources.Resources.slow
        Me.slow2.Location = New System.Drawing.Point(217, 334)
        Me.slow2.Name = "slow2"
        Me.slow2.Size = New System.Drawing.Size(28, 23)
        Me.slow2.TabIndex = 11
        '
        'stun2
        '
        Me.stun2.Image = Global.Bomberman.My.Resources.Resources.Stunned
        Me.stun2.Location = New System.Drawing.Point(183, 334)
        Me.stun2.Name = "stun2"
        Me.stun2.Size = New System.Drawing.Size(28, 23)
        Me.stun2.TabIndex = 10
        '
        'stun1
        '
        Me.stun1.Image = Global.Bomberman.My.Resources.Resources.Stunned
        Me.stun1.Location = New System.Drawing.Point(79, 334)
        Me.stun1.Name = "stun1"
        Me.stun1.Size = New System.Drawing.Size(28, 23)
        Me.stun1.TabIndex = 9
        '
        'GameArea
        '
        Me.GameArea.BackColor = System.Drawing.SystemColors.ControlText
        Me.GameArea.BackgroundImage = Global.Bomberman.My.Resources.Resources._2
        Me.GameArea.Location = New System.Drawing.Point(-1, -2)
        Me.GameArea.Margin = New System.Windows.Forms.Padding(0)
        Me.GameArea.Name = "GameArea"
        Me.GameArea.Size = New System.Drawing.Size(325, 321)
        Me.GameArea.TabIndex = 0
        Me.GameArea.TabStop = False
        Me.GameArea.Visible = False
        '
        'MainScreen
        '
        Me.MainScreen.BackColor = System.Drawing.SystemColors.ControlText
        Me.MainScreen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.MainScreen.Location = New System.Drawing.Point(-1, -1)
        Me.MainScreen.Name = "MainScreen"
        Me.MainScreen.Size = New System.Drawing.Size(325, 323)
        Me.MainScreen.TabIndex = 1
        Me.MainScreen.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(188, 334)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 13)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Score :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(229, 334)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(13, 13)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "0"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.ClientSize = New System.Drawing.Size(319, 356)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.slow1)
        Me.Controls.Add(Me.slow2)
        Me.Controls.Add(Me.stun2)
        Me.Controls.Add(Me.stun1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.life2)
        Me.Controls.Add(Me.life1)
        Me.Controls.Add(Me.gotomenu)
        Me.Controls.Add(Me.ext)
        Me.Controls.Add(Me.restart)
        Me.Controls.Add(Me.GameArea)
        Me.Controls.Add(Me.MainScreen)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BomberGIRL"
        CType(Me.GameArea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MainScreen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ImageList As System.Windows.Forms.ImageList
    Friend WithEvents Timer As System.Windows.Forms.Timer
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    Friend WithEvents MainScreen As System.Windows.Forms.PictureBox
    Friend WithEvents GameArea As System.Windows.Forms.PictureBox
    Friend WithEvents restart As System.Windows.Forms.Button
    Friend WithEvents ext As System.Windows.Forms.Button
    Friend WithEvents gotomenu As System.Windows.Forms.Button
    Friend WithEvents life1 As System.Windows.Forms.Label
    Friend WithEvents life2 As System.Windows.Forms.Label
    Friend WithEvents timerPlayer1 As System.Windows.Forms.Timer
    Friend WithEvents timerPlayer2 As System.Windows.Forms.Timer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents stun1 As System.Windows.Forms.Label
    Friend WithEvents stun2 As System.Windows.Forms.Label
    Friend WithEvents slow2 As System.Windows.Forms.Label
    Friend WithEvents slow1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label

End Class
