﻿Public Class clsTrap
    Enum Trap
        Stun = 0
        Slow = 1
    End Enum

    Public IsEmpty As Boolean = True
    Public TrapType As Trap


    Public Sub InsertTrap(ByVal Type As Trap)
        IsEmpty = False
        TrapType = Type
    End Sub


    Public Sub RemoveTrap()
        IsEmpty = True
    End Sub
End Class
