Public Module General

    Public imgFireEast(8) As Bitmap
    Public imgFireWest(8) As Bitmap
    Public imgFireNorth(8) As Bitmap
    Public imgFireSouth(8) As Bitmap
    Public imgFireHorizontal(8) As Bitmap
    Public imgFireVertical(8) As Bitmap
    Public imgFireCenter(8) As Bitmap
    Public imgBomb1 As Bitmap
    Public imgBomb2 As Bitmap
    Public imgBrick As Bitmap
    Public imgFireBricks(5) As Bitmap
    Public imgWall As Bitmap
    Public imgBonusFire As Bitmap
    Public imgBonusBomb As Bitmap

    Public Const BLOCK_WIDTH = 16
    Public Const BLOCK_HEIGHT = 16

    Public Const BOMBERMAN_WIDTH = 16
    Public Const BOMBERMAN_HEIGHT = 25

    Public BomberMan(4) As clsBomberman

    Public TotalPlayers As Integer

    Public GameTimer As Long
    Public GameOver As Boolean



   
    Function GetBlockFromXY(ByVal X As Integer, ByVal Y As Integer) As Integer
        Dim BlocksFromTop As Integer
        Dim BlocksFromLeft As Integer

        BlocksFromTop = Y / BLOCK_HEIGHT
        BlocksFromLeft = X / BLOCK_WIDTH

        Return (BlocksFromTop * frmMain.BLOCKS_PER_LINE) + BlocksFromLeft

    End Function


    Function GetXYFromBlock(ByVal Block As Integer) As Point
        Dim BlocksFromTop As Integer
        Dim BlocksFromLeft As Integer
        Dim XY As Point

        BlocksFromLeft = (Block Mod frmMain.BLOCKS_PER_LINE)
        BlocksFromTop = Math.Floor(Block / frmMain.BLOCKS_PER_LINE)

        XY.X = BlocksFromLeft * BLOCK_WIDTH
        XY.Y = BlocksFromTop * BLOCK_HEIGHT

        Return XY

    End Function


    Public Sub EmptyGrids()

        For i As Integer = 0 To frmMain.TOTAL_BLOCKS
            frmMain.BricksGrid(i).RemoveBrick()
            frmMain.WallGrid(i).RemoveWall()
            frmMain.BonusGrid(i).RemoveBonus()
            frmMain.BombGrid(i).RemoveBomb()
            frmMain.FireGrid(i).RemoveFire()
            frmMain.TrapGrid(i).RemoveTrap()
        Next

    End Sub

    Public Sub FillWalls()

        For i As Integer = 0 To frmMain.TOTAL_BLOCKS

            If (i \ frmMain.BLOCKS_PER_LINE) Mod 2 = 0 Then frmMain.WallGrid(i).InsertWall()
            If i Mod 2 = 1 Then frmMain.WallGrid(i).RemoveWall()

            If (i Mod frmMain.BLOCKS_PER_LINE = 0) Or (i Mod frmMain.BLOCKS_PER_LINE = frmMain.BLOCKS_PER_LINE - 1) _
             Or (i < frmMain.BLOCKS_PER_LINE) Or (i > frmMain.TOTAL_BLOCKS - frmMain.BLOCKS_PER_LINE) Then
                frmMain.WallGrid(i).InsertWall()
            End If
        Next

    End Sub

    Public Sub FillBricks()

        Dim r As New Random()
        Dim x As Integer

        For i As Integer = 0 To frmMain.TOTAL_BLOCKS
            frmMain.BricksGrid(i).InsertBrick()
        Next i

        For i As Integer = 0 To frmMain.TOTAL_BLOCKS

            If (frmMain.WallGrid(i).IsEmpty = True) Then
                x = r.Next(0, 2)
                If x = 0 Then
                    frmMain.BricksGrid(i).RemoveBrick()

                End If
            End If

        Next i


        frmMain.BricksGrid(frmMain.BLOCKS_PER_LINE + 1).RemoveBrick()
        frmMain.BricksGrid(frmMain.BLOCKS_PER_LINE + 2).RemoveBrick()
        frmMain.BricksGrid(2 * frmMain.BLOCKS_PER_LINE + 1).RemoveBrick()


        frmMain.BricksGrid((frmMain.BLOCKS_PER_LINE - 3) + frmMain.BLOCKS_PER_LINE * (frmMain.BLOCKS_PER_LINE - 2)).RemoveBrick()
        frmMain.BricksGrid((frmMain.BLOCKS_PER_LINE - 2) + frmMain.BLOCKS_PER_LINE * (frmMain.BLOCKS_PER_LINE - 3)).RemoveBrick()
        frmMain.BricksGrid((frmMain.BLOCKS_PER_LINE - 2) + frmMain.BLOCKS_PER_LINE * (frmMain.BLOCKS_PER_LINE - 2)).RemoveBrick()

        If ((frmMain.SCRN_WIDTH / BLOCK_WIDTH) Mod 2 = 0) Then
            frmMain.WallGrid((frmMain.BLOCKS_PER_LINE - 2) + frmMain.BLOCKS_PER_LINE * (frmMain.BLOCKS_PER_LINE - 2)).RemoveWall()
        End If

    End Sub



    Public Sub FillBonus()

        Dim TotalBonuses As Integer = 6 * TotalPlayers
        Dim r As New Random()
        Dim x As Integer

        For i As Integer = 1 To TotalBonuses
            Do
                x = r.Next(0, frmMain.TOTAL_BLOCKS)
            Loop Until ((frmMain.BricksGrid(x).IsEmpty = False) And (frmMain.WallGrid(x).IsEmpty = True) And (frmMain.BonusGrid(x).IsEmpty = True))

            frmMain.BonusGrid(x).InsertBonus(clsBonus.Bonuses.BOMB)

            Do
                x = r.Next(0, frmMain.TOTAL_BLOCKS)
            Loop Until ((frmMain.BricksGrid(x).IsEmpty = False) And (frmMain.WallGrid(x).IsEmpty = True) And (frmMain.BonusGrid(x).IsEmpty = True))

            frmMain.BonusGrid(x).InsertBonus(clsBonus.Bonuses.FIRE)
        Next


    End Sub

    Public Sub FillTrap()

        Dim TotalTrap As Integer = 2 * TotalPlayers
        Dim r As New Random()
        Dim x As Integer

        For i As Integer = 1 To TotalTrap
            Do
                x = r.Next(0, frmMain.TOTAL_BLOCKS)
            Loop Until ((frmMain.WallGrid(x).IsEmpty = True) And (frmMain.TrapGrid(x).IsEmpty = True))

            frmMain.TrapGrid(x).InsertTrap(clsTrap.Trap.Stun)

            Do
                x = r.Next(0, frmMain.TOTAL_BLOCKS)
            Loop Until ((frmMain.WallGrid(x).IsEmpty = True) And (frmMain.TrapGrid(x).IsEmpty = True))

            frmMain.TrapGrid(x).InsertTrap(clsTrap.Trap.Slow)
        Next
    End Sub



    Public Sub InitializeBombermans()

        For i As Integer = 0 To 1
            BomberMan(i).Strength = 1
            BomberMan(i).BombsLeft = 1
            BomberMan(i).State = 0
            BomberMan(i).IsAlive = True
        Next


        BomberMan(0).X = 16
        BomberMan(0).Y = 16
        BomberMan(0).Direction = clsBomberman.Directions.DOWN


        BomberMan(1).X = frmMain.SCRN_WIDTH - 32
        BomberMan(1).Y = frmMain.SCRN_HEIGHT - 42
        BomberMan(1).Direction = clsBomberman.Directions.UP


    End Sub


    Public Sub LoadKeyConfiguration()

        BomberMan(0).KEY_UP = Keys.Up
        BomberMan(0).KEY_DOWN = Keys.Down
        BomberMan(0).KEY_LEFT = Keys.Left
        BomberMan(0).KEY_RIGHT = Keys.Right
        BomberMan(0).KEY_BOMB = Keys.Enter

        BomberMan(1).KEY_UP = Keys.W
        BomberMan(1).KEY_DOWN = Keys.S
        BomberMan(1).KEY_LEFT = Keys.A
        BomberMan(1).KEY_RIGHT = Keys.D
        BomberMan(1).KEY_BOMB = Keys.Space



    End Sub

    Public Sub InitializeArrays()
        For i As Integer = 0 To frmMain.TOTAL_BLOCKS
            frmMain.BombGrid(i) = New clsBombs
            frmMain.WallGrid(i) = New clsWalls
            frmMain.BricksGrid(i) = New clsBricks
            frmMain.FireGrid(i) = New clsFire
            frmMain.BonusGrid(i) = New clsBonus
            frmMain.TrapGrid(i) = New clsTrap
        Next
        For i As Integer = 0 To 3
            BomberMan(i) = New clsBomberman
        Next
    End Sub

    Function IsBombermanColliding(ByVal x As Integer, ByVal y As Integer) As Boolean

        Dim XY As Point

        For i As Integer = 0 To frmMain.TOTAL_BLOCKS
            If (frmMain.BricksGrid(i).IsEmpty = False) Or (frmMain.WallGrid(i).IsEmpty = False) Then
                XY = GetXYFromBlock(i)
                If ((y + 14 <= XY.Y + BLOCK_HEIGHT) And (y + BOMBERMAN_HEIGHT > XY.Y)) And ((x + BOMBERMAN_WIDTH >= XY.X + 4) And (x + 4 <= XY.X + BLOCK_WIDTH)) Then
                    Return True
                End If
            End If
        Next

        Return False

    End Function


    Function IsBombermanCollidingWithFire(ByVal x As Integer, ByVal y As Integer) As Boolean

        Dim XY As Point

        For i As Integer = 0 To frmMain.TOTAL_BLOCKS
            If frmMain.FireGrid(i).IsEmpty = False Then
                XY = GetXYFromBlock(i)
                If ((y + 14 <= XY.Y + BLOCK_HEIGHT) And (y + BOMBERMAN_HEIGHT > XY.Y)) And ((x + BOMBERMAN_WIDTH >= XY.X) And (x <= XY.X + BLOCK_WIDTH)) Then
                    Return True
                End If
            End If
        Next

        Return False

    End Function


End Module